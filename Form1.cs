﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Productos.Models;

namespace Productos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Refrescar();
        }

        #region HELPER
        private void Refrescar()
        {
            using (BDEntities db = new BDEntities())
            {
                var lst = from d in db.productos
                          select d;
                dataGridView1.DataSource = lst.ToList();
            }
        }
        #endregion

        private void Button1_Click(object sender, EventArgs e)
        {
            Presentacion.frmProducto ofrmProducto = new Presentacion.frmProducto();
            ofrmProducto.ShowDialog();
            Refrescar();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            int id = obtenerId();
            if (id != null)
            {
                Presentacion.frmProducto ofrmProducto = new Presentacion.frmProducto(id);
                ofrmProducto.ShowDialog();

                Refrescar();
            }
        }

        public int obtenerId()
        {
                return int.Parse(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString());
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            int id = obtenerId();
            if (id != null)
            {
                using (BDEntities db = new BDEntities())
                {
                    productos oProductos = db.productos.Find(id);
                    db.productos.Remove(oProductos);

                    db.SaveChanges();
                }

                    Refrescar();
            }
        }
    }
}
