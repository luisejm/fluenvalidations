﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Productos.Models;

namespace Productos.Presentacion
{
    public partial class frmProducto : Form
    {
        public int? id;
        productos oProductos = null;
        public frmProducto(int? id = null)
        {
            InitializeComponent();
            this.id = id;

            if (id != null)
            {
                cargarDatos();
            }
        }

        private void cargarDatos()
        {
            using (BDEntities db = new BDEntities())
            {
                oProductos = db.productos.Find(id);
                txtNombre.Text = oProductos.nombre;
                txtPrecio.Text = Convert.ToString(oProductos.precio);
                txtDescripcion.Text = oProductos.descripcion;
            }
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            using (BDEntities db = new BDEntities())
            {

                if (id == null)
                {
                    productos oProductos = new productos();
                }
                
                oProductos.nombre = txtNombre.Text;
                oProductos.precio = Decimal.Parse(txtPrecio.Text);
                oProductos.descripcion = txtDescripcion.Text;

                if (id == null)
                {
                    db.productos.Add(oProductos);
                }
                else
                {
                    db.Entry(oProductos).State = System.Data.Entity.EntityState.Modified;
                }
                
                db.SaveChanges();
                this.Close();

            }
        }

        private void FrmProducto_Load(object sender, EventArgs e)
        {

        }
    }
}
