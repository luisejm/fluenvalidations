CREATE TABLE productos
(id INT PRIMARY KEY NOT NULL,
nombre VARCHAR(25) NOT NULL,
precio MONEY NULL,
descripcion TEXT NULL)
GO

INSERT INTO productos (id, nombre, precio, descripcion) VALUES ('2', 'Queso', '800', 'Dos Pinos');

SELECT * FROM productos;